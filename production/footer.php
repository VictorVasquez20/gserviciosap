      
      </div>
    </div>

    <!-- jQuery -->
    <script src="../public/build/js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../public//build/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../public/build/js/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../public/build/js/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../public/build/js/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../public/build/js/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../public/build/js/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../public/build/js/icheck.min.js"></script>


    <!-- bootstrap-fileinput -->
    <script src="../public/build/js/fileinput.min.js"></script>
    <!-- bootstrap-select -->
    <script src="../public/build/js/bootstrap-select.min.js"></script>

    <!-- Datatables -->
    <script src="../public/build/js/jquery.dataTables.min.js"></script>
    <script src="../public/build/js/dataTables.bootstrap.min.js"></script>
    <script src="../public/build/js/dataTables.buttons.min.js"></script>
    <script src="../public/build/js/buttons.bootstrap.min.js"></script>
    <script src="../public/build/js/buttons.flash.min.js"></script>
    <script src="../public/build/js/buttons.html5.min.js"></script>
    <script src="../public/build/js/buttons.print.min.js"></script>
    <script src="../public/build/js/dataTables.fixedHeader.min.js"></script>
    <script src="../public/build/js/dataTables.keyTable.min.js"></script>
    <script src="../public/build/js/dataTables.responsive.min.js"></script>
    <script src="../public/build/js/responsive.bootstrap.js"></script>
    <script src="../public/build/js/dataTables.scroller.min.js"></script>
    <script src="../public/build/js/jszip.min.js"></script>
    <script src="../public/build/js/pdfmake.min.js"></script>
    <script src="../public/build/js/vfs_fonts.js"></script>

    <!-- Bootbox Alert -->
    <script src="../public/build/js/bootbox.min.js"></script>

    <!-- jquery.inputmask -->
    <script src="../public/build/js/jquery.inputmask.bundle.min.js"></script>

    
    <!-- Switchery -->
    <script src="../public/build/js/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="../public/build/js/select2.full.min.js"></script>
    <!-- Autosize -->
    <script src="../public/build/js/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../public/build/js/jquery.autocomplete.min.js"></script>
    
    <!-- PNotify -->
    <script src="../public/build/js/pnotify.js"></script>
    <script src="../public/build/js/pnotify.buttons.js"></script>
    <script src="../public/build/js/pnotify.nonblock.js"></script>
    
    <!-- morris.js -->
    <script src="../public/build/js/raphael.min.js"></script>
    <script src="../public/build/js/morris.min.js"></script>
    
    <!-- ECharts -->
    <script src="../public/build/js/echarts.min.js"></script>

    <!-- DateJS -->
    <script src="../public/build/js/date.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../public/build/js/custom.js"></script>

    <div id="loadingDiv"><img src="../public/build/images/gif-sap.gif" id="imgLoading"></div>
    <script>
        $(document).ajaxStart(function () {
          $('#loadingDiv').show();
        }).ajaxStop(function () {
          $('#loadingDiv').hide();
        });
    </script>
	
  </body>
</html>