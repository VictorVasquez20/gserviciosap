var tabla;
var refdata, refdataant;
var ctx, mybarChart;
var ctxant, mybarChartant;


//funcion que se ejecuta iniciando
function init() {
    CargarDatos();
    CargarGraficos();
    ListarAlerta();
    setInterval("Actualizar()", 10000);
}

function ListarAlerta() {
        $("#Charts").show();
}


function CargarGraficos() {

    $.post("../ajax/estado.php?op=DatosGrafico", function (data, status) {
        data = JSON.parse(data);
        refdata = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        for (var i = 0; i < data.aaData.length; i++) {
            refdata[data.aaData[i][0] - 1] = data.aaData[i][1];
        }
        

        ctx = document.getElementById("mybarChart");
        ctx.height = 125;
        mybarChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                datasets: [{
                        label: 'SERVICIOS EN EL AÑO',
                        backgroundColor: "#26B99A",
                        data: refdata
                    }]
            },

            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });
    });

}

function ActializarGraficos() {

    $.post("../ajax/estado.php?op=DatosGrafico", function (data, status) {
        data = JSON.parse(data);

        var updata = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        for (var i = 0; i < data.aaData.length; i++) {
            updata[data.aaData[i][0] - 1] = data.aaData[i][1];
        }

        for (var i = 0; i < updata.length; i++) {
            if (refdata[i] != updata[i]) {
                refdata[i] = updata[i];
                console.log("Consiguio diferencia");
                console.log(refdata);
                mybarChart.data.datasets[0]['data']=refdata;
                mybarChart.update();
            }

        }
    });
}

function CargarDatos() {

    $.post("../ajax/estado.php?op=ContarDatos", function (data, status) {
        data = JSON.parse(data);
        //Actualizamos valores
        $("#num_año").html(data.año.servicios);
        $("#num_mes").html(data.mes.servicios);
        $("#num_dia").html(data.dia.servicios);
    });
}

function Actualizar() {
    CargarDatos();
    ActializarGraficos();
}

init();