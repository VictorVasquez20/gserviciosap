<?php
//functions: rearrange_array_attachments
//funcion que reorganiza o reordena el array $_FILES
//dejando TODOS los adjuntos de un formulario de esta forma:
/*
Array
(
    [0] => Array
        (
            [name] => SERV Personalizado oct.xls
            [type] => application/vnd.ms-excel
            [tmp_name] => C:\wamp64\tmp\phpF53A.tmp
            [error] => 0
            [size] => 204800
        )
    [1] => Array
        (
            ...
        )
    [2] => Array
        (
            ...
        )
    etc
)
*/
    function rearrange_array_attachments($arr) {
        $indice = 0;
        $files = array();
        foreach ($arr as $fieldname => $keys)
        {
            if (is_array($keys['name'])) {
                foreach ($keys as $key => $list)
                {
                    foreach ($list as $no => $value) {
                        if (!$keys['error'][$no])
                            $files[$indice + $no][$key] = $value;
                    }
                }
            }
            else {
                if (!$keys['error'])
                    $files[] = $keys;
            }
            if (count($files))
                $indice = max(array_keys($files)) + 1;
        }

        //si hay saltos de indices, reordena partiendo del indice 0
        $files = array_values(array_filter($files));
        return $files;
    }

?>