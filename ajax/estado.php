<?php 
session_name('SESS_GSAP');
session_start();
require_once "../modelos/Estado.php";


$estado = new Estado();

switch ($_GET["op"]) {
        
    case 'ContarDatos':
        $iduser=$_SESSION['iduser'];
        $año = $estado->serviciosaño($iduser);
        $mes = $estado->serviciosmes($iduser);
        $dia = $estado->serviciosdia($iduser);
        $results = array(
                "año"=>$año,
                "mes"=>$mes, 
                "dia"=>$dia
            );
        echo json_encode($results);
        break;
        
    case 'DatosGrafico':
        $iduser=$_SESSION['iduser'];
        $rspta=$estado->contarservicios($iduser);
        $data = Array();
        while ($reg = $rspta->fetch_object()){
            $data[] = array(
                "0"=>$reg->mes,
                "1"=>$reg->servicios
            );
        }
        $results = array(
            "Totalservicios"=>count($data),
            "aaData"=>$data
        );
        
        echo json_encode($results);
        break;
     
}

 ?>