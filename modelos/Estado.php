<?php 

	require "../config/conexion.php";

	Class Estado{
		//Constructor para instancias
		public function __construct(){
		}

		public function serviciosaño($iduser){
			$sql="SELECT COUNT(idservicio) AS servicios FROM servicio WHERE YEAR(created_time)=YEAR(NOW()) AND iduser='$iduser' AND estadofin IS NOT NULL";
			return ejecutarConsultaSimpleFila($sql);
		}

		public function serviciosmes($iduser){
			$sql="SELECT COUNT(idservicio) AS servicios FROM servicio WHERE YEAR(created_time)=YEAR(NOW()) AND MONTH(created_time) = MONTH(NOW()) AND iduser='$iduser' AND estadofin IS NOT NULL";
			return ejecutarConsultaSimpleFila($sql);
		}

		public function serviciosdia($iduser){
			$sql="SELECT COUNT(idservicio) AS servicios FROM servicio WHERE DATE(created_time)=DATE(NOW()) AND iduser='$iduser' AND  estadofin IS NOT NULL";
			return ejecutarConsultaSimpleFila($sql);
		}

		public function contarservicios($iduser){
			$sql="SELECT MONTH(created_time) AS mes, COUNT(idservicio) AS servicios FROM servicio WHERE YEAR(created_time)=YEAR(NOW()) AND iduser='$iduser' AND estadofin IS NOT NULL GROUP BY MONTH(created_time)";
			return ejecutarConsulta($sql);
		}
	}
?>